package br.com.fatecararas.listsexemplo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        String[] novosFilmes = {"Matrix","Matrix Reloaded", "Matrix Revolutions"};

        listView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, novosFilmes));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Posição: "+position+" - "+
                            ((TextView)view).getText().toString(),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}
